[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![license](https://img.shields.io/npm/l/@kkitahara/unicode-tools.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![version](https://img.shields.io/npm/v/@kkitahara/unicode-tools/latest.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)
[![bundle size](https://img.shields.io/bundlephobia/min/@kkitahara/unicode-tools.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)
[![downloads per week](https://img.shields.io/npm/dw/@kkitahara/unicode-tools.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)
[![downloads per month](https://img.shields.io/npm/dm/@kkitahara/unicode-tools.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)
[![downloads per year](https://img.shields.io/npm/dy/@kkitahara/unicode-tools.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)
[![downloads total](https://img.shields.io/npm/dt/@kkitahara/unicode-tools.svg)](https://www.npmjs.com/package/@kkitahara/unicode-tools)

[![pipeline status](https://gitlab.com/kkitahara/unicode-tools/badges/v1.0.8/pipeline.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/v1.0.8)
[![coverage report](https://gitlab.com/kkitahara/unicode-tools/badges/v1.0.8/coverage.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/v1.0.8)

[![pipeline status](https://gitlab.com/kkitahara/unicode-tools/badges/master/pipeline.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/master)
[![coverage report](https://gitlab.com/kkitahara/unicode-tools/badges/master/coverage.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/master)
(master)

[![pipeline status](https://gitlab.com/kkitahara/unicode-tools/badges/develop/pipeline.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/develop)
[![coverage report](https://gitlab.com/kkitahara/unicode-tools/badges/develop/coverage.svg)](https://gitlab.com/kkitahara/unicode-tools/commits/develop)
(develop)

# UnicodeTools

ECMAScript modules which implement some of the Unicode&reg; Standard.

Currently, only the default/canonical case folding
(the Unicode&reg; Standard 12.0.0, section 3.13, R4/D145) is implemented.

New functionalities may be added on (author's) demand in future.

## Installation

```
npm install @kkitahara/unicode-tools
```

## Examples

```javascript
import { toCaseFold } from '@kkitahara/unicode-tools'

'\u{00c5}' === '\u{00e5}' // false
toCaseFold('\u{00c5}') === '\u{00e5}' // true

'\u{0041}\u{030a}' === '\u{0061}\u{030a}' // false
toCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' // true

// return the input value if it is not a string
toCaseFold(null) // null
toCaseFold(false) // false
```

```javascript
import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'

'\u{00c5}' === '\u{00e5}' // false
toCanonicalCaseFold('\u{00c5}') === '\u{00e5}' // false
toCanonicalCaseFold('\u{00c5}') === '\u{0061}\u{030a}' // true

'\u{0041}\u{030a}' === '\u{0061}\u{030a}' // false
toCanonicalCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' // true

// return the input value if it is not a string
toCanonicalCaseFold(null) // null
toCanonicalCaseFold(false) // false
```

## NOTICE
This software uses data extracted from the Unicode&reg; Character Database.

See [NOTICE](NOTICE)
and the [Unicode Data Files and Software License](LICENSE_UNICODE)
for more details.

Unicode is a registered trademark of Unicode, Inc. in the United States and
other countries.

## LICENSE
&copy; 2019 Koichi Kitahara  
[Apache 2.0](LICENSE)
