v1.0.8:
- toCaseFold and toCanonicalCaseFold now returns the input `value`
  if it is not a string.
  This is now considered as an intended behaviour.
  Future changes which break this behaviour shall be major changes.

v1.0.7:
- toCaseFold and toCanonicalCaseFold now throws an `Error`
  if the input `str` is not a string.

v1.0.6:
- toCaseFold and toCanonicalCaseFold now returns `undefined`
  if the input `str` is not a string.

v1.0.5:
- Add .npmignore
- Add CHNAGELOG.md
- Add .gitlab-ci.yml
- Add nyc and esm
- Updat README.md
