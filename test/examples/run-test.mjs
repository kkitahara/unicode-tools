import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import './src_toCaseFold.mjs_toCaseFold-example0.mjs'
import './src_toCaseFold.mjs_toCanonicalCaseFold-example0.mjs'

testDriver.showSummary()
if (testDriver.fail) {
  process.exit(1)
}
