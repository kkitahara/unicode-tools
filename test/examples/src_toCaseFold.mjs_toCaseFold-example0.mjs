import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { toCaseFold } from '../../src/index.mjs'

testDriver.test(() => { return '\u{00c5}' === '\u{00e5}' }, false, 'src/toCaseFold.mjs~toCaseFold-example0_0', false)
testDriver.test(() => { return toCaseFold('\u{00c5}') === '\u{00e5}' }, true, 'src/toCaseFold.mjs~toCaseFold-example0_1', false)

testDriver.test(() => { return '\u{0041}\u{030a}' === '\u{0061}\u{030a}' }, false, 'src/toCaseFold.mjs~toCaseFold-example0_2', false)
testDriver.test(() => { return toCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' }, true, 'src/toCaseFold.mjs~toCaseFold-example0_3', false)

// return the input value if it is not a string
testDriver.test(() => { return toCaseFold(null) }, null, 'src/toCaseFold.mjs~toCaseFold-example0_4', false)
testDriver.test(() => { return toCaseFold(false) }, false, 'src/toCaseFold.mjs~toCaseFold-example0_5', false)
