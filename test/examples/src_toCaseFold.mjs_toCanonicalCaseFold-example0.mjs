import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { toCanonicalCaseFold } from '../../src/index.mjs'

testDriver.test(() => { return '\u{00c5}' === '\u{00e5}' }, false, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_0', false)
testDriver.test(() => { return toCanonicalCaseFold('\u{00c5}') === '\u{00e5}' }, false, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_1', false)
testDriver.test(() => { return toCanonicalCaseFold('\u{00c5}') === '\u{0061}\u{030a}' }, true, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_2', false)

testDriver.test(() => { return '\u{0041}\u{030a}' === '\u{0061}\u{030a}' }, false, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_3', false)
testDriver.test(() => { return toCanonicalCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' }, true, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_4', false)

// return the input value if it is not a string
testDriver.test(() => { return toCanonicalCaseFold(null) }, null, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_5', false)
testDriver.test(() => { return toCanonicalCaseFold(false) }, false, 'src/toCaseFold.mjs~toCanonicalCaseFold-example0_6', false)
