/**
 * @source: https://www.npmjs.com/package/@kkitahara/unicode-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @desc
 * this is an auxiliary script to extract data required for
 * the canonical caseless matching from the Unicode&reg; Character Database.
 *
 * See NOTICE and LICENSE_UNICODE for more detail.
 *
 * Unicode is a registered trademark of Unicode, Inc. in the United States
 * and other countries.
 */

import fs from 'fs'

const lines = fs.readFileSync('CaseFolding.txt').toString().split('\n')

const map = {}
let rePattern = ''
for (let i = 0; i < lines.length; i += 1) {
  if (lines[i].length > 0 && lines[i][0] !== '#') {
    const line = lines[i].split(/ *; */g)
    const stat = line[1]
    if (stat === 'C' || stat === 'F') {
      const key = '\\u{' + line[0] + '}'
      const mapping = line[2].split(/ +/g)
      let mappingStr = ''
      for (let j = 0; j < mapping.length; j += 1) {
        mappingStr += '\\u{' + mapping[j] + '}'
      }
      map[key] = mappingStr
      rePattern += key
    }
  }
}

let str = JSON.stringify({ map: map, rePattern: rePattern })
str = str.replace(/"/g, '\'').replace(/:/g, ': ').replace(/,/g, ',\n  ')
console.log(str)

/* @license-end */
