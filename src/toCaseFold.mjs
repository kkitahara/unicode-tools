/**
 * @source: https://www.npmjs.com/package/@kkitahara/unicode-tools
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { map, re } from './caseFolding.mjs'

/**
 * @desc
 * This is an implementation of the default case folding which complies
 * with the Unicode&reg; Standard (12.0.0, section 3.13, R4).
 *
 * @param {String} value
 * a string to be case-folded.
 *
 * @return {String}
 * the result of the default case folding,
 * or `value` if `value` is not a {@link String}.
 *
 * @example
 * import { toCaseFold } from '@kkitahara/unicode-tools'
 *
 * '\u{00c5}' === '\u{00e5}' // false
 * toCaseFold('\u{00c5}') === '\u{00e5}' // true
 *
 * '\u{0041}\u{030a}' === '\u{0061}\u{030a}' // false
 * toCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' // true
 *
 * // return the input value if it is not a string
 * toCaseFold(null) // null
 * toCaseFold(false) // false
 *
 * @version
 * 1.0.8
 *
 * @since
 * 1.0.0
 */
export function toCaseFold (value) {
  if (typeof value === 'string') {
    return value.replace(re, match => {
      return map.get(match)
    })
  } else {
    return value
  }
}

/**
 * @desc
 * This is an implementation of the mapping
 * `NFD(toCaseFold(NFD(str)))` (here, we call it canonical case folding)
 * used for the canonical caseless matching which complies with
 * the Unicode&reg; Standard (12.0.0, section 3.13, D145).
 * For the normalisation `NFD(str)`, built-in
 * `String.prototype.normalize('NFD')` is used.
 *
 * @param {String} value
 * a string to be canonical case-folded.
 *
 * @return {String}
 * the result of the canonical case folding of `value`,
 * or `value` if `value` is not a {@link String}.
 *
 * @example
 * import { toCanonicalCaseFold } from '@kkitahara/unicode-tools'
 *
 * '\u{00c5}' === '\u{00e5}' // false
 * toCanonicalCaseFold('\u{00c5}') === '\u{00e5}' // false
 * toCanonicalCaseFold('\u{00c5}') === '\u{0061}\u{030a}' // true
 *
 * '\u{0041}\u{030a}' === '\u{0061}\u{030a}' // false
 * toCanonicalCaseFold('\u{0041}\u{030a}') === '\u{0061}\u{030a}' // true
 *
 * // return the input value if it is not a string
 * toCanonicalCaseFold(null) // null
 * toCanonicalCaseFold(false) // false
 *
 * @version
 * 1.0.8
 *
 * @since
 * 1.0.0
 */
export function toCanonicalCaseFold (value) {
  if (typeof value === 'string') {
    return toCaseFold(value.normalize('NFD')).normalize('NFD')
  } else {
    return value
  }
}

/* @license-end */
